const mongoose = require('mongoose');

const flux = mongoose.Schema({
    _id: {type: String, required: true},
    title: { type: String, required: true },
    link: { type: String, required: true  },
    description: { type: String, required: true },
    image: { type: String, default: null },
    pubDate: { type: String, required: true },
    channel_title: { type: String, required: true },
    channel_link: { type: String, required: true },
    channel_image: { type: String, default: null }
});

module.exports = mongoose.model('flux', flux);