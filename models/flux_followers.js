const mongoose = require('mongoose');

const followers = mongoose.Schema({
  user_id: { type: String, required: true },
  rss_url: { type: String, required: true },
  update: { type: String, default: "2000-01-01" },
});

module.exports = mongoose.model('followers', followers);