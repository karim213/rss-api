const Flux = require('../models/flux');
const FluxFollowers = require('../models/flux_followers');
const mongoose = require('mongoose');
const axios = require('axios');
var parserHtml = require('node-html-parser');
const randomstring = require("randomstring");


// --- Connexion A la base de données
mongoose.connect('mongodb+srv://rss_user:rsssport21@cluster0.cgn4e.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
	.then(() => console.log('Connexion à MongoDB réussie !'))
	.catch(() => console.log('Connexion à MongoDB échouée !'));


// --- Récuperation image article
function getImageArticle(item){
    var description = item.description[0].replace("<![CDATA[", "").replace("]]>", "");
    if(parserHtml.parse(description).querySelector('img')){
        return parserHtml.parse(description).querySelector('img').getAttribute('src');
    }
    if(item.enclosure){
        return item.enclosure[0].url[0];
    }

    return null;
}

// --- Recuperation liste des Articles
exports.getArticles = function (req, res) {
	if (!req.query.articleId) {
		Flux.find().sort({
				'pubDate': -1
			})
			.then(flux => res.status(200).json(flux))
			.catch(error => res.status(400).json({
				error
			}));
	} else {
		Flux.findOne({
				"_id": req.query.articleId
			})
			.then(flux => res.status(200).json(flux))
			.catch(error => res.status(400).json({
				error
			}));
	}
}

// --- Ajout nouveau flux Rss
exports.addFlux = function (req, res) {
	const flux_followers = new FluxFollowers({
		user_id: req.body.userId,
		rss_url: req.body.rssUrl
	});
	flux_followers.save()
		.then(() => res.status(201).json({
			message: 'Flux enregistrer avec succés !'
		}))
		.catch(error => res.status(400).json({
			error
		}));
}

// --- Mettre a jour les donnéees de bdd
exports.update = function (req, res) {
	const xml2js = require('xml2js');
	FluxFollowers.find() // --- Récuperation des Flux enregistrés
		.then(follows => {
			follows.map(follow => { // --- Pour chaque Flux enregistrés
				axios.get(follow.rss_url).then(function (response) { // --- Récuperation des données du flux rss
					xml2js.parseString(response.data, {
						mergeAttrs: true
					}, (err, result) => {
						if (err) {
							throw err;
						}
						const response_json = JSON.parse(JSON.stringify(result)); // --- parser la réponse Xml en Json
						response_json.rss.channel[0].item.map((item) => { // -- Parcours des résultats

							if ((new Date(follow.update)).getTime() < (new Date(item.pubDate[0])).getTime()) { // -- Si le flux n'est pas déja enregistrer (on verifie par rapport au temps de la derniere maj)
								// --- Récuperation de l'image de l'article
								image_url = getImageArticle(item);
								// --- Enregistrement de l'article dans la bdd
								const flux = new Flux({
									_id: randomstring.generate(),
									title: item.title[0],
									link: item.link[0],
									description: item.description[0].replace(/<img[^>]*>/g, "").replace("<![CDATA[", "").replace("]]>", ""),
									image: image_url,
									pubDate: item.pubDate[0],
									channel_title: response_json.rss.channel[0].title[0],
									channel_link: response_json.rss.channel[0].link[0],
									channel_image: response_json.rss.channel[0].image[0].url[0]
								});
								flux.save()
							}
						});

					});

				});
				// -- Mise a jour de la table des abonnements 
				FluxFollowers.updateOne({
					rss_url: follow.rss_url
				}, {
					user_id: follow.user_id,
					rss_url: follow.rss_url,
					update: new Date()
				})
				.then((obj) => {
                })
                .catch((err) => {
                    console.log('Error update');
                })
			});
		})
		.then(() => res.status(201).json({
			message: 'Articles enregistrés !'
		}))
		.catch(error => res.status(400).json({
			error
		}));

}
