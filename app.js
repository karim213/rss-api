const express = require('express');
const bodyParser = require("body-parser");
const Actions = require('./services/actions');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.post('/api/follow', (req, res) => {
    Actions.addFlux(req, res);
});

app.get('/api/follow', (req, res) => {
    Actions.getArticles(req, res);
});

app.get('/api/news', (req, res) => {
    Actions.update(req, res);
});

module.exports = app;